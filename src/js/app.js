import 'jquery.global.js';
import fancybox from '@fancyapps/fancybox';
import page from 'page';
import forms from 'forms';
import slick from 'slick-carousel';
import {
  disableBodyScroll,
  enableBodyScroll,
  clearAllBodyScrollLocks
} from 'body-scroll-lock';

import Swiper from 'swiper/bundle';

import mainPage from 'index.page';


let app = {


  scrollToOffset: 200, // оффсет при скролле до элемента
  scrollToSpeed: 500, // скорость скролла

  init: function () {
    // read config
    if (typeof appConfig === 'object') {
      Object.keys(appConfig).forEach(key => {
        if (Object.prototype.hasOwnProperty.call(app, key)) {
          app[key] = appConfig[key];
        }
      });
    }

    app.currentID = 0;

    // Init page
    this.page = page;
    this.page.init.call(this);

    this.forms = forms;
    this.forms.init.call(this);

    // Init page

    this.mainPage = mainPage;
    this.mainPage.init.call(this);

    //window.jQuery = $;
    window.app = app;

    app.document.ready(() => {


    });



    app.window.on('load', () => {


      let $slider = document.querySelectorAll('.js-screen-wrap');
      let $sliderItem = document.querySelectorAll('.slider-screen')
      let $pic = document.querySelectorAll('.pic-slider');
      let $topText = document.querySelectorAll('.general-slide-wrap');
      let $sliderAbout = document.querySelector('.slider-about-us');
      let $scrollDiv = document.querySelector('.news-inner-list');
      let $preload = document.querySelector('.preloader');
      let $mainContainer = document.querySelector('.slider-main');

      $preload.classList.add('active');

      setTimeout(() => {
        $preload.style.cssText = "display:none"
      }, 2200)

      var swiper = new Swiper('.swiper-container', {
        init: false,
        direction: 'horizontal',
        slidesPerView: 1,
        spaceBetween: 30,
        mousewheel: true,
        parallax: true,
        pagination: {
          el: '.swiper-pagination',
          clickable: true,
          dynamicBullets: true,
        },
        navigation: {
          nextEl: '.swiper-button-next',
          prevEl: '.swiper-button-prev',
        },

      });

      setTimeout(() => {
        swiper.on('init', function () {
          // do something
          $mainContainer.classList.add('active');
          $pic.forEach((item => {
            item.classList.add('animate')
          }))

          $slider.forEach((item => {
            item.classList.add('animate');
            item.addEventListener('transitionend', transitionEndText, false);

            function transitionEndText() {
              item.classList.remove('animate');
              item.classList.add('clear');

              setTimeout(() => {
                item.classList.remove('clear');
              }, 600);
            }
          }))

          $slider.forEach((item => {
            item.classList.add('test');

          }))

          $topText.forEach((item => {
            item.classList.add('test2');
          }))
        });
        swiper.init();
      }, 2000)


      swiper.on('slideChange', () => {
        $pic.forEach((item => {
          item.classList.remove('animate')
        }))

        $sliderItem.forEach((item => {
          item.classList.add('animate');
          item.addEventListener('transitionend', onTransitionEnd, false);

          function onTransitionEnd() {
            setTimeout(() => {
              item.classList.add('clear');
              item.classList.remove('animate')
            }, 200);

            setTimeout(() => {
              item.classList.remove('clear');
            }, 400);
          }
        }))

        $slider.forEach((item => {
          item.classList.add('animate');
          item.addEventListener('transitionend', transitionEndText, false);

          function transitionEndText() {
            item.classList.remove('animate');
            item.classList.add('clear');

            setTimeout(() => {
              item.classList.remove('clear');
            }, 600);
          }
        }))
      })

      swiper.on('setTransition', () => {
        $slider.forEach((item => {
          item.classList.remove('test');
        }))

        $slider.forEach((item => {
          item.classList.remove('animate');
        }))

        $topText.forEach((item => {
          item.classList.remove('test2');
        }))

      })

      swiper.on('slideChangeTransitionStart', () => {
        $pic.forEach((item => {
          item.classList.remove('animate')
        }))
      })

      swiper.on('slideChangeTransitionEnd', () => {
        $slider.forEach((item => {
          item.classList.add('test');

        }))

        $pic.forEach((item => {
          item.classList.toggle('animate')
        }))

        $topText.forEach((item => {
          item.classList.add('test2');
        }))

        $sliderAbout.classList.add('test2')


      })


      let $burger = document.querySelector('.js-burger');
      let $menu = document.querySelector('.js-menu');
      let $close = document.querySelector('.js-close');

      $burger.addEventListener('click', (e) => {
        e.stopPropagation();
        openMenu();
      })

      document.addEventListener('click', (e) => {
        let target = e.target;
        if (!$menu.contains(target)) {
          closeMenu();
        }

      })

      $close.addEventListener('click', () => {
        if ($menu.classList.contains('active')) {
          closeMenu();
        }
      })

      function openMenu() {
        $menu.classList.add('active')
        disableBodyScroll($menu);
      }

      function closeMenu() {
        $menu.classList.remove('active');
        clearAllBodyScrollLocks();
      }


    });

    // this.document.on(app.resizeEventName, () => {
    // });

  },

};
app.init();